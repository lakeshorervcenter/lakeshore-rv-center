Lakeshore RV Center located in Muskegon, Michigan in Muskegon County is a Nationwide RV dealer located in West Michigan. So whether you're as close as Grand Rapids or across the country we can deliver to your door, 5th Wheels, Travel Trailers, Toy Haulers, Destination Trailers and other Camper RVs.

Website: https://lakeshore-rv.com/
